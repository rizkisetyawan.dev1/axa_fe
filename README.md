## Installation
- Install axa_fe from github
```bash
  git clone https://gitlab.com/rizkisetyawan.dev1/axa_fe.git
  cd axa_fe
```
- Install Docker, https://docs.docker.com
- Run axa_fe with docker compose
```bash
  docker compose up -d --build
```
- open the browser and access, http://localhost:8080
