import { Burger, Container, Group, Image } from '@mantine/core';
import { useDisclosure } from '@mantine/hooks';
import { NavLink, Outlet } from "react-router-dom";
import classes from './Header.module.css';

const links = [
  { to: '/posts', label: 'Posts' },
  { to: '/albums', label: 'Albums' },
  { to: '/', label: 'Users' },
];

function Header() {
  const [opened, { toggle }] = useDisclosure(false);

  const items = links.map((link) => {
    return (
      <NavLink
        key={link.label}
        to={link.to}
        className={classes.link}
        onClick={() => toggle()}
        style={({ isActive, isTransitioning }) => {
          return {
            fontWeight: isActive ? "bold" : "",
            color: isActive ? "#228be6" : "",
            viewTransitionName: isTransitioning ? "slide" : "",
          };
        }}

      >
        {link.label}
      </NavLink>
    );
  });

  return (
    <>
      <header className={classes.header}>
        <Container size="md">
          <div className={classes.inner}>
            <Image src="./axa-image.png" w={150} />
            <Group gap={5} visibleFrom="sm">
              {items}
            </Group>
            <Burger opened={opened} onClick={toggle} size="sm" hiddenFrom="sm" />
          </div>
        </Container>
      </header>
      <main>
        <Container size="md" mb="xl">
          <Outlet />
        </Container>
      </main>
    </>
  );
}

export default Header;
