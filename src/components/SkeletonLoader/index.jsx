import { Grid, Skeleton } from '@mantine/core';
import PropTypes from 'prop-types';
import React from 'react';

const SkeletonLoader = ({ count, height, withGrid }) => {
  return Array.from({ length: count }, (_, index) => {
    if (withGrid) {
      return (
        <Grid.Col span={{ base: 12, md: 6, lg: 4 }} key={index}>
          <Skeleton height={height} radius="sm" />
        </Grid.Col>
      )
    }
    return (
      <Skeleton key={index} height={height} radius="sm" mb="md" />
    )
  });
};

SkeletonLoader.propTypes = {
  count: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  withGrid: PropTypes.bool,
};

SkeletonLoader.defaultProps = {
  withGrid: false,
};

export default SkeletonLoader;
