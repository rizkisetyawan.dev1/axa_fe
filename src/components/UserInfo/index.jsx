import { Avatar, Card, Group, Text } from '@mantine/core';
import { IconAt, IconPhoneCall } from '@tabler/icons-react';
import PropTypes from 'prop-types';
import classes from './UserInfo.module.css';
import { fetchUserPhoto } from '../../services';

function UserInfo({
  id,
  catchPhrase,
  name,
  email,
  phone,
}) {
  return (
    <Card shadow="sm" radius="md">
      <Group wrap="nowrap">
        <Avatar
          src={fetchUserPhoto(id)}
          size={90}
          radius="md"
        />
        <div>
          <Text fz="xs" tt="uppercase" fw={700} c="dimmed">
            {catchPhrase}
          </Text>

          <Text fz="lg" fw={500} className={classes.name}>
            {name}
          </Text>

          <Group wrap="nowrap" gap={10} mt={3}>
            <IconAt stroke={1.5} size="1rem" className={classes.icon} />
            <Text fz="xs" c="dimmed">
              {email}
            </Text>
          </Group>

          <Group wrap="nowrap" gap={10} mt={5}>
            <IconPhoneCall stroke={1.5} size="1rem" className={classes.icon} />
            <Text fz="xs" c="dimmed">
              {phone}
            </Text>
          </Group>
        </div>
      </Group>
    </Card>
  );
}

export default UserInfo;

UserInfo.propTypes = {
  id: PropTypes.number.isRequired,
  catchPhrase: PropTypes.string,
  name: PropTypes.string.isRequired,
  email: PropTypes.string,
  phone: PropTypes.string,
};

UserInfo.defaultProps = {
  catchPhrase: 'No catchphrase available',
  email: 'No email available',
  phone: 'No phone number available',
};

