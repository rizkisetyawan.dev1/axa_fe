import {
  ActionIcon,
  Box,
  Divider,
  Flex,
  Group,
  Select,
  Text,
  Title,
  Tooltip,
} from '@mantine/core';
import { IconPhoto } from '@tabler/icons-react';
import React, { useEffect, useMemo, useState } from 'react';
import { useQuery } from 'react-query';
import { useNavigate } from 'react-router-dom';
import SkeletonLoader from '../../components/SkeletonLoader';
import fetchData from '../../services/fetchData';

const extractUserData = (data) => data.map((user) => ({ value: `${user.id}`, label: user.name }));

function Albums() {
  const navigate = useNavigate();
  const [selectedUser, setSelectedUser] = useState(null);

  const queryUsers = useQuery(['users'], () => fetchData('users'), {
    onSuccess: (data) => {
      if (data.length > 0) {
        setSelectedUser(extractUserData(data)[0]);
      }
    },
  });

  const dataUsers = useMemo(() => extractUserData(queryUsers.data || []), [queryUsers.data]);

  const queryAlbums = useQuery(
    ['albums', selectedUser?.value],
    () => fetchData(`albums?userId=${selectedUser?.value}`),
    {
      enabled: !!selectedUser,
    }
  );

  const handleToPhotos = (album) => {
    navigate(`/albums/${album.id}`, { state: { title: album.title } });
  };

  useEffect(() => {
    if (!selectedUser && dataUsers.length > 0) {
      setSelectedUser(dataUsers[0]);
    }
  }, [dataUsers, selectedUser]);

  return (
    <>
      <Group mb="lg" justify="space-between">
        <Title>Albums</Title>
        <Select
          searchable
          placeholder="Select Users"
          data={dataUsers}
          value={selectedUser?.value}
          onChange={(value, option) => setSelectedUser(option)}
        />
      </Group>

      {queryAlbums.isLoading && <SkeletonLoader count={8} height={126} />}

      {queryAlbums.isError && (
        <Text color="red">Failed to load albums. Please try again later.</Text>
      )}

      {!queryAlbums.isLoading &&
        !queryAlbums.isError &&
        queryAlbums.data &&
        queryAlbums.data.map((album) => (
          <Box key={album.id}>
            <Flex justify="space-between">
              <Text>{album.title}</Text>
              <Tooltip label="View Photos">
                <ActionIcon variant="light" onClick={() => handleToPhotos(album)}>
                  <IconPhoto size={18} />
                </ActionIcon>
              </Tooltip>
            </Flex>
            <Divider my="md" />
          </Box>
        ))}
    </>
  );
}

export default Albums;
