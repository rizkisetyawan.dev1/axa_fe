import { Grid, Image, Text, Title } from '@mantine/core';
import React from 'react';
import { useLocation, useParams } from 'react-router-dom';
import fetchData from '../../services/fetchData';
import { useQuery } from 'react-query';
import { SkeletonLoader } from '../../components';
import { modals } from '@mantine/modals';

function Photos() {
  const { albumId } = useParams();
  const { state } = useLocation();
  const queryPhotos = useQuery(['photos', albumId], () => fetchData('photos', { query: { albumId } }));

  const handleOpenDetailPhoto = (photo) => {
    modals.open({
      size: 'lg',
      title: photo.title,
      children: <Image src={photo.url} width={600} />
    })
  }

  return (
    <>
      <Title mb="lg">Photos - {state.title}</Title>
      <Grid gutter={4}>
        {queryPhotos.isLoading && <SkeletonLoader count={8} height={150} withGrid />}

        {queryPhotos.isError && (
          <Grid.Col span={12}>
            <Text c="red">Failed to load users. Please try again later.</Text>
          </Grid.Col>
        )}

        {!queryPhotos.isLoading &&
          !queryPhotos.isError &&
          queryPhotos.data &&
          queryPhotos.data.map((photo) => (
            <Grid.Col key={photo.id} span={{ base: 4, md: 3, lg: 2 }}>
              <Image
                style={{ cursor: 'pointer' }}
                src={photo.thumbnailUrl}
                onClick={() => handleOpenDetailPhoto(photo)}
              />
            </Grid.Col>
          ))}
      </Grid>
    </>
  )
}


export default Photos