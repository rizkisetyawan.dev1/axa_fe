import { ActionIcon, Avatar, Box, Button, Container, Divider, Flex, Group, Text, TextInput, Title } from '@mantine/core';
import { useForm } from '@mantine/form';
import { modals } from '@mantine/modals';
import { notifications } from '@mantine/notifications';
import { IconDeviceFloppy, IconPencil, IconPlus, IconTrash } from '@tabler/icons-react';
import React, { Fragment, useEffect, useState } from 'react';
import { useMutation, useQuery } from 'react-query';
import { useLocation, useParams } from 'react-router-dom';
import { SkeletonLoader } from '../../components';
import { fetchData, fetchUserPhoto } from '../../services';

function CommentForm({ action, data, onAddComment, onEditComment }) {
  const isAddComment = action === 'Add';
  const form = useForm({
    initialValues: {
      postId: data.postId,
      name: "",
      email: "",
      body: "",
    },
  });

  const { mutate, isLoading } = useMutation(
    (values) =>
      fetchData(isAddComment ? "comments" : `comments/${data.id}`, {
        method: isAddComment ? "POST" : "PUT",
        body: values,
      }),
    {
      onSuccess: (data) => {
        form.reset();
        modals.closeAll();
        notifications.show({
          color: "green",
          message: `Successfully ${action} Comment`,
        });

        if (isAddComment) {
          onAddComment(data);
        } else {
          onEditComment(data)
        }
      },
    }
  );

  const handleChangeInput = (event, name) => {
    form.setFieldValue(name, event.currentTarget.value);
  };

  const handleSubmit = (values) => {
    mutate(values);
  };

  useEffect(() => {
    if (action === "Edit") {
      form.setValues(data);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <form onSubmit={form.onSubmit(handleSubmit)}>
      <TextInput
        required
        label="Name"
        value={form.values.name}
        onChange={(event) => handleChangeInput(event, "name")}
      />
      <TextInput
        required
        label="Email"
        mt="xs"
        value={form.values.email}
        onChange={(event) => handleChangeInput(event, "email")}
      />
      <TextInput
        required
        label="Body"
        mt="xs"
        value={form.values.body}
        onChange={(event) => handleChangeInput(event, "body")}
      />
      <Group justify="flex-end" mt="md">
        <Button
          type="submit"
          loading={isLoading}
          leftSection={<IconDeviceFloppy />}
        >
          Save
        </Button>
      </Group>
    </form>
  )
}

function PostDetail() {
  const { postId } = useParams();
  const { state } = useLocation();
  const [comments, setComments] = useState([]);
  const queryComments = useQuery(['comments', state.postId], () => fetchData('comments', { query: { postId } }), {
    onSuccess: (data) => {
      setComments(data)
    }
  });

  const handleAddComment = (newComment) => {
    setComments((currentComments) => ([
      ...currentComments,
      newComment
    ]))
  }

  const handleEditComment = (newComment) => {
    setComments((currentComments) => (
      currentComments.map((comment) => {
        if (comment.id === newComment.id) {
          return newComment;
        }
        return comment;
      })
    ))
  }

  const handleRemoveComment = (commentId) => {
    setComments((currentComments) => (currentComments.filter((comment) => comment.id !== commentId)))
    notifications.show({
      color: "red",
      message: `Successfully Delete Comment`,
    });
  }

  const handleOpenForm = (action, data) => {
    modals.open({
      title: `${action} Comment`,
      children: (
        <CommentForm
          action={action}
          data={data}
          onAddComment={handleAddComment}
          onEditComment={handleEditComment}
        />
      )
    })
  }

  return (
    <>
      <Title mb="lg">Post Detail</Title>
      <Flex gap="lg">
        <Avatar src={fetchUserPhoto(state.userId)} size={40} radius={20} mt="xs" />
        <Box>
          <Title order={3} c="blue" fw={700}>{state.title}</Title>
          <Title order={4} c="black.5" fw={400}>{state.body}</Title>
          <Divider my="md" />
        </Box>
      </Flex>
      <Container size="sm" ml="sm">
        {queryComments.isLoading && <SkeletonLoader count={3} height={150} />}

        {queryComments.isError && (
          <Text c="red">Failed to load users. Please try again later.</Text>
        )}

        {!queryComments.isLoading &&
          !queryComments.isError &&
          queryComments.data && (
            <>
              <Title order={3} mb="lg">Comments ({comments.length})</Title>
              {
                comments.map((post) => (
                  <Fragment key={post.id}>
                    <Group justify="space-between">
                      <Box>
                        <Text fz="sm">{post.name}</Text>
                        <Text fz="xs" c="dimmed">
                          {post.email}
                        </Text>
                      </Box>
                      <Group>
                        <ActionIcon variant="transparent" onClick={() => handleOpenForm('Edit', post)}>
                          <IconPencil size={16} color='black' />
                        </ActionIcon>
                        <ActionIcon variant="transparent" onClick={() => handleRemoveComment(post.id)}>
                          <IconTrash size={16} color='black' />
                        </ActionIcon>
                      </Group>
                    </Group>
                    <Text>{post.body}</Text>
                    <Divider my="sm" />
                  </Fragment>
                ))
              }
              <Group mt="md">
                <Button
                  color='green'
                  onClick={() => handleOpenForm('Add', { postId })}
                  leftSection={<IconPlus stroke={4} size={14} />}
                >
                  New Comment
                </Button>
              </Group>
            </>
          )}
      </Container>
    </>
  )
}

export default PostDetail