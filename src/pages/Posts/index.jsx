import { ActionIcon, Avatar, Box, Button, Divider, Flex, Group, Select, Text, TextInput, Title, Tooltip } from '@mantine/core';
import { useForm } from '@mantine/form';
import { modals } from '@mantine/modals';
import { notifications } from '@mantine/notifications';
import { IconDeviceFloppy, IconEye, IconPencil, IconPlus, IconTrash } from '@tabler/icons-react';
import { useEffect, useMemo, useState } from 'react';
import { useMutation, useQuery } from 'react-query';
import { SkeletonLoader } from '../../components';
import { useNavigate } from 'react-router-dom';
import { fetchUserPhoto, fetchData } from '../../services';

function PostForm({ action, data, onAddPost, onEditPost }) {
  const isAddPost = action === 'Add';
  const form = useForm({
    initialValues: {
      userId: "",
      title: "",
      body: "",
    },
  });

  const { mutate, isLoading } = useMutation(
    (values) =>
      fetchData(isAddPost ? "posts" : `posts/${data.id}`, {
        method: isAddPost ? "POST" : "PUT",
        body: values,
      }),
    {
      onSuccess: (data) => {
        form.reset();
        modals.closeAll();
        notifications.show({
          color: "green",
          message: `Successfully ${action} Post`,
        });

        if (isAddPost) {
          onAddPost(data);
        } else {
          onEditPost(data)
        }
      },
    }
  );

  const handleChangeInput = (event, name) => {
    form.setFieldValue(name, event.currentTarget.value);
  };

  const handleSubmit = (values) => {
    mutate(values);
  };

  useEffect(() => {
    if (action === "Edit") {
      form.setValues(data);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <form onSubmit={form.onSubmit(handleSubmit)}>
      <TextInput
        required
        label="ID User"
        value={form.values.userId}
        onChange={(event) => handleChangeInput(event, "userId")}
      />
      <TextInput
        required
        label="Title"
        mt="xs"
        value={form.values.title}
        onChange={(event) => handleChangeInput(event, "title")}
      />
      <TextInput
        required
        label="Body"
        mt="xs"
        value={form.values.body}
        onChange={(event) => handleChangeInput(event, "body")}
      />
      <Group justify="flex-end" mt="md">
        <Button
          type="submit"
          loading={isLoading}
          leftSection={<IconDeviceFloppy />}
        >
          Save
        </Button>
      </Group>
    </form>
  )
}

const extractUserData = (data) => data.map((user) => ({ value: `${user.id}`, label: user.name }));

function Posts() {
  const navigate = useNavigate();
  const [posts, setPosts] = useState([]);
  const [selectedUser, setSelectedUser] = useState(null);

  const queryUsers = useQuery(['users'], () => fetchData('users'), {
    onSuccess: (data) => {
      if (data.length > 0) {
        setSelectedUser(extractUserData(data)[0]);
      }
    },
  });

  const dataUsers = useMemo(() => extractUserData(queryUsers.data || []), [queryUsers.data]);

  const queryPosts = useQuery(['posts', selectedUser?.value], () => fetchData(`posts?userId=${selectedUser?.value}`), {
    enabled: !!selectedUser,
    onSuccess: (data) => {
      setPosts(data)
    }
  });

  const handleAddPost = (newPost) => {
    setPosts((currentPosts) => ([
      ...currentPosts,
      newPost
    ]))
  }

  const handleEditPost = (newPost) => {
    setPosts((currentPosts) => (
      currentPosts.map((post) => {
        if (post.id === newPost.id) {
          return newPost;
        }
        return post;
      })
    ))
  }

  const handleRemovePost = (postId) => {
    setPosts((currentPosts) => (currentPosts.filter((post) => post.id !== postId)))
    notifications.show({
      color: "red",
      message: `Successfully Delete Post`,
    });
  }

  const handleOpenForm = (action, data) => {
    modals.open({
      title: `${action} Post`,
      children: (
        <PostForm
          action={action}
          data={data}
          onAddPost={handleAddPost}
          onEditPost={handleEditPost}
        />
      )
    })
  }

  const handleToDetailPost = (post) => {
    navigate(`/posts/${post.id}`, { state: post })
  }

  useEffect(() => {
    if (!selectedUser && dataUsers.length > 0) {
      setSelectedUser(dataUsers[0]);
    }
  }, [dataUsers, selectedUser]);


  return (
    <>
      <Group mb="xl" justify="space-between">
        <Title>Posts</Title>
        <Group>
          <Select
            searchable
            placeholder="Select Users"
            data={dataUsers}
            value={selectedUser?.value}
            onChange={(value, option) => setSelectedUser(option)}
          />
          <Button
            color='green'
            onClick={() => handleOpenForm('Add')}
            leftSection={<IconPlus stroke={4} size={14} />}
          >
            New Post
          </Button>
        </Group>
      </Group>
      {queryPosts.isLoading && <SkeletonLoader height={140} count={8} />}

      {queryPosts.isError && (
        <Text c="red">Failed to load users. Please try again later.</Text>
      )}

      {!queryPosts.isLoading &&
        !queryPosts.isError &&
        queryPosts.data &&
        posts.map((post) => (
          <Flex key={post.id} gap="lg">
            <Avatar src={fetchUserPhoto(post.userId)} size={40} radius={20} mt="xs" />
            <Box >
              <Title order={3} c="blue" fw={700}>{post.title}</Title>
              <Title order={4} c="black.5" fw={400}>{post.body}</Title>
              <Group mt={0} justify="flex-end">
                <Tooltip label="Detail">
                  <ActionIcon variant="transparent" onClick={() => handleToDetailPost(post)}>
                    <IconEye size={20} color='black' />
                  </ActionIcon>
                </Tooltip>
                <Tooltip label="Edit">
                  <ActionIcon variant="transparent" onClick={() => handleOpenForm('Edit', post)}>
                    <IconPencil size={20} color='black' />
                  </ActionIcon>
                </Tooltip>
                <Tooltip label="Delete">
                  <ActionIcon variant="transparent" onClick={() => handleRemovePost(post.id)}>
                    <IconTrash size={20} color='black' />
                  </ActionIcon>
                </Tooltip>
              </Group>
              <Divider my="md" />
            </Box>
          </Flex>
        ))}
    </>
  )
}

export default Posts;
