import { Grid, Text, Title } from '@mantine/core';
import React from 'react';
import { useQuery } from 'react-query';
import { SkeletonLoader, UserInfo } from '../../components';
import fetchData from '../../services/fetchData';


function Users() {
  const queryUsers = useQuery(['users'], () => fetchData('users'));

  return (
    <>
      <Title ml="lg" mb="lg">Users</Title>
      <Grid gutter="lg">
        {queryUsers.isLoading && <SkeletonLoader count={8} height={126} withGrid />}

        {queryUsers.isError && (
          <Grid.Col span={12}>
            <Text c="red">Failed to load users. Please try again later.</Text>
          </Grid.Col>
        )}

        {!queryUsers.isLoading &&
          !queryUsers.isError &&
          queryUsers.data &&
          queryUsers.data.map((user) => (
            <Grid.Col
              key={user.id}
              span={{ base: 12, md: 6, lg: 4 }}
            >
              <UserInfo
                id={user.id}
                name={user.name}
                phone={user.phone}
                email={user.email}
                catchPhrase={user.company.catchPhrase}
              />
            </Grid.Col>
          ))}
      </Grid>
    </>
  );
}

export default Users;
