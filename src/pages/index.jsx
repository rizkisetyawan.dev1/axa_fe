import Albums from './Albums';
import Photos from './Photos';
import PostDetail from './PostDetail';
import Posts from './Posts';
import Users from './Users';

export {
  Albums, Photos, PostDetail, Posts, Users
};
