import { createBrowserRouter } from "react-router-dom";
import { Users, Posts, Albums, Photos, PostDetail } from "../pages";
import { Header } from "../components";

const route = createBrowserRouter([
  {
    path: "/",
    element: <Header />,
    children: [
      {
        element: <Users />,
        index: true
      },
      {
        path: "/posts",
        element: <Posts />,
      },
      {
        path: "/posts/:postId",
        element: <PostDetail />,
      },
      {
        path: "/albums",
        element: <Albums />,
      },
      {
        path: "/albums/:albumId",
        element: <Photos />,
      },
    ]
  }
]);

export default route;
