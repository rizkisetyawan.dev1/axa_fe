async function fetchData(
  path,
  { method = "GET", query = {}, body = null } = {}
) {
  try {
    const requestOptions = {
      method,
      headers: {
        "Content-Type": "application/json",
      },
    };

    if (method !== "GET" && body) {
      requestOptions.body = JSON.stringify(body);
    }

    const filteredQuery = Object.fromEntries(
      Object.entries(query).filter(
        ([, value]) => value !== "" && value !== null
      )
    );
    const queryParams = new URLSearchParams(filteredQuery);
    const queryString = queryParams.toString();

    const response = await fetch(
      `https://jsonplaceholder.typicode.com/${path}${queryString && `?${queryString}`}`,
      requestOptions
    );

    const data = await response.json();
    if (!response.ok) {
      throw new Error(data.errors || "Error fetching data");
    }
    return data;
  } catch (err) {
    throw err.message;
  }
}

export default fetchData;