const fetchUserPhoto = (id) => `https://raw.githubusercontent.com/mantinedev/mantine/master/.demo/avatars/avatar-${id}.png`;

export default fetchUserPhoto;