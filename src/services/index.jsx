import fetchData from './fetchData';
import fetchUserPhoto from './fetchUserPhoto';

export {
  fetchData,
  fetchUserPhoto,
}